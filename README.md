# awx_quick_install

Just a quick install method for awx using ansible

Build to run on CentOS 8 managed minimal server.

#### Requirements

None

#### Variables

## Dependencies

None

#### Example

#### License

MIT

#### Contributor Information ( Welcome more Contributor )

* Lee Thoong Ching

#### Feedback, bug-reports, requests, ...

Are [welcome](https://gitlab.com/tc.lee/awx_quick_install)!